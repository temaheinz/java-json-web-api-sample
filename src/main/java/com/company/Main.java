package com.company;

import Domain.ProductsRepository;
import Helpers.HttpHelper;
import Domain.Models.Product;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Collection;

public class Main {

    public static void main(String[] args) {
        ProductsRepository repository = new ProductsRepository();
        System.out.printf("Эта программа выводит список продуктов.").println();

        try {
            //repository.update(11, "Игровой контроллер");
            Collection<Product> products = repository.getAll();
            for(Product item: products){
                //String s = "Id: " + item.Id + ", Наименование: " + item.Name;
                //System.out.println(s);

                System.out.printf("Id: %s, Наименование: %s", item.Id, item.Name).println();
            }

        }catch (Exception ex){
            System.out.printf(ex.getMessage());
        }
        System.out.printf("Конец");
    }

}
